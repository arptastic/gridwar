module.exports = function(grunt) {

  // Project configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true,
          $: true
        },
      },
      ignore_warning: {
        options: {
          '-W057': true
        },
        src: ['./app.js', './html/script.js']
      },
      all: ['./app.js'/*, './html/script.js'*/]
    },
    watch: {
      scripts: {
        files: ['**/*.js', '**/*.css', '**/*.html'],
        tasks: ['default'],
        options: {
          spawn: false,
        },
      },
    },
    server: {
      port: 8000,
      base: './html'
    }
  });

  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default tasks
  grunt.registerTask('default', ['jshint']);

  // Server tasks
  grunt.registerTask('server', 'Start server', function() {
    grunt.log.writeln('Started server on 8000');
    require('./app.js');
  });

  // Watch tasks
  grunt.registerTask('watch');

};