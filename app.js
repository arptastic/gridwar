var port = 8000;
var pfWidth = 16, pfHeight = 16;
var regen = 1000;

var app = require('http').createServer(handler),
	io = require('socket.io').listen(app, { log: false }),
	fs = require('fs')
;

app.listen(port);

var childProcess = require('child_process');
var grunt = childProcess.spawn('grunt', ['--force', 'default', 'watch']);

grunt.stdout.on('data', function(data) {
	console.log("%s", data);
});

function handler (req, res) {
	var url = req.url;
	
	if (url === '/') {
		url = 'index.html';
	}
	
	fs.readFile('html/' + url, function (error, data) {
		if (error) {
			res.writeHead(500);
			return res.end('Error, yo.');
		}
		
		res.writeHead(200);
		res.end(data);
	});
}
var idCounter = 0;

var playfield = [];

for (var x = 0; x < pfWidth; x++) {
	for (var y = 0; y < pfHeight; y++) {
		playfield.push({i: playfield.length, x: x, y: y, owner: null, pop: 0});
	}
}

io.sockets.on('connection', function (socket) {
	var id = idCounter++; 
	
	socket.on('move', function(data){
		var source = playfield[data.sourceId];
		var target = playfield[data.targetId];
		
		// are source and target in proximity?
		
		var xVar = source.x - target.x;
		var yVar = source.y - target.y;
		
		if (xVar < -1 || xVar > 1 || yVar < -1 || yVar > 1) {
			return;
		}
		
		var sourceToMove = source.pop - 1;
		
		if (source.owner === target.owner) {
			// transferring units to friendly
			var targetMissing = 9 - target.pop;
			
			if (sourceToMove > targetMissing) {
				sourceToMove = targetMissing;
			}
			
			target.pop += sourceToMove;
		} else {
			// attacking hostile / neutral
			target.pop -= sourceToMove;
			
			if (target.pop === 0) {
				target.owner = null;
			}
			
			if (target.pop < 0) {
				target.owner = source.owner;
				target.pop = -target.pop;
			}
		}
		
		source.pop -= sourceToMove;
		
		io.sockets.emit('sector', source);
		io.sockets.emit('sector', target);
	});
	
	socket.on('disconnect', function () {
		for (var i in playfield) {
			var item = playfield[i];
			
			if (item.owner === id) {
				item.owner = null;
				item.pop = 0;
			}
		}
	});
	
	socket.emit('init', { w: pfWidth, h: pfHeight, id: id });
	
	var start = false;
	
	while (!start) {
		var item = playfield[Math.floor(Math.random() * playfield.length)];
		
		if (item.owner === null) {
			item.owner = id;
			item.pop = 1;
			start = true;
		}
	}
	
	// initial send of entire playfield
	socket.emit('pf', playfield);
});

var update = function () {
	for (var i in playfield) {
		var item = playfield[i];
		
		if (item.owner !== null && item.pop < 9) {
			item.pop++;
			io.sockets.emit('sector', item);
		}
	}
	
	setTimeout(update, regen);
};

update();
