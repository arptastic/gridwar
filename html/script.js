var viewModel = new (function() {
	var self = this;
	self.socket = io.connect('/');
	self.pfWidth = ko.observable();
	self.pfHeight = ko.observable();
	self.playfield = ko.observableArray();
	self.id = ko.observable();
	self.selectedSector = ko.observable();
	
	self.sectorClick = function(data) {
		if (data.owner === self.id()) {
			//selected an owned sector
			self.selectedSector(data.i);
		} else if (self.selectedSector()) {
			//own sector selected already, attempt move
			var sourceSectorId = self.selectedSector();
			var sourceSector = self.playfield()[sourceSectorId];
			var available = sourceSector.pop - 1;
			
			if (available > 0) {
				self.socket.emit('move', { sourceId: sourceSectorId, targetId: data.i});
				self.selectedSector(null);
			}
		} 
	};
	
	self.getSectorClass = function(data, pop, x, y) {
		var ownerClass;
		var popClass = 'p-'+pop;
		var selectedClass = data.i === self.selectedSector() ? 'selected' : '';
		var xClass = 'x-'+x;
		var yClass = 'y-'+y;
		switch (data.owner) {
			case null:
			ownerClass = 'empty';
			break;
			case self.id():
			ownerClass = 'me';
			break;
			default:
			ownerClass = 'enemy';
			break;
		}
		return 's '+ownerClass + ' ' + popClass + ' ' + selectedClass + ' ' + xClass + ' ' + yClass;
	};
	
	//self.getSelectedClass = function(data) {
	//	if (data.i === self.selectedSector()) {
	//		return 'selected';
	//	}
		
	//	return '#000';
	//};
	
	self.socket.on('init', function(data) {
		self.pfWidth(data.w);
		self.pfHeight(data.h);
		self.id(data.id);
	});
	
	self.socket.on('pf', function(data) {
		self.playfield(data);
	});
	
	self.socket.on('sector', function(data) {
		var oldSector = self.playfield()[data.i];
		self.playfield.replace(oldSector, data);
		/* select new sector if you've just taken ownership
		if (oldSector.owner != self.id() && data.owner == self.id()) {
			self.selectedSector(data);
		}	*/
	});
})();

ko.applyBindings(viewModel, document.getElementById('container'));
